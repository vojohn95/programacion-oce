@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reportes
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    @if (Auth::user()->id_rol != '3')
                    <h5 style="padding-left: 20px;">Reporte promesas</h5>
                        {{ Form::open(['action' => ['PromesaController@export'], 'method' => 'POST']) }}
                        <div class="form-group col-sm-4">
                            {!! Form::label('inicio', 'Inicio:') !!}
                            {!! Form::text('inicio', null, ['class' => 'form-control', 'id' => 'inicio', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::label('fin', 'Fin:') !!}
                            {!! Form::text('fin', null, ['class' => 'form-control', 'id' => 'fin', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-4" style="padding-top: 25px;">
                            <button type="submit" class="btn btn-sm btn-primary form-control">Consultar</button>
                        </div>
                        {!! Form::close() !!}
                    @endif

                    <h5 style="padding-left: 20px;">Reporte Clientes</h5>
                    {{ Form::open(['action' => ['PromesaController@exportCliente'], 'method' => 'POST']) }}
                    <div class="form-group col-sm-4">
                        {!! Form::label('inicio', 'Inicio:') !!}
                        {!! Form::text('inicio', null, ['class' => 'form-control', 'id' => 'inicioc', 'required']) !!}
                    </div>
                    <div class="form-group col-sm-4">
                        {!! Form::label('fin', 'Fin:') !!}
                        {!! Form::text('fin', null, ['class' => 'form-control', 'id' => 'finc', 'required']) !!}
                    </div>
                    <div class="form-group col-sm-4" style="padding-top: 25px;">
                        <button type="submit" class="btn btn-sm btn-primary form-control">Consultar</button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#inicio').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: true,
                sideBySide: true
            })
        </script>

        <script type="text/javascript">
            $('#fin').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: true,
                sideBySide: true
            })
        </script>

        <script type="text/javascript">
            $('#inicioc').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: true,
                sideBySide: true
            })
        </script>

        <script type="text/javascript">
            $('#finc').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: true,
                sideBySide: true
            })
        </script>
    @endpush

@endsection
