<div class="table-responsive">
    <table class="table" id="myTable">
        <thead>
            <tr>
                {{-- <th>Grupo</th> --}}
                <th>Proyecto</th>
                <th>Cliente</th>
                <th>Estacionamiento</th>
                <th>Concepto</th>
                <th>Rfc</th>
                <th>Monto Cxp</th>
                <th>Fecha Entrega</th>
                <th>Estatus</th>
                <th>Observaciones</th>
                <th>Gerente</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rentaPendientes as $rentaPendiente)
                <tr>
                    {{-- <td>{{ $rentaPendiente->grupo }}</td> --}}
                    <td>{{ $rentaPendiente->proyecto }}</td>
                    <td>{{ $rentaPendiente->id_cliente }}</td>
                    <td>{{ $rentaPendiente->estacionamiento }}</td>
                    <td>{{ $rentaPendiente->concepto }}</td>
                    <td>{{ $rentaPendiente->rfc }}</td>
                    <td>{{ $rentaPendiente->monto_cxp }}</td>
                    <td>{{ $rentaPendiente->fecha_entrega }}</td>
                    <td>{{ $rentaPendiente->estatus }}</td>
                    <td>{{ $rentaPendiente->observaciones }}</td>
                    <td>{{ $rentaPendiente->gerente }}</td>


                    {!! Form::open(['route' => ['rentaPendientes.destroy', $rentaPendiente->id_rentas], 'method' => 'delete']) !!}
                    {{-- <a href="{{ route('rentaPendientes.show', [$rentaPendiente->id_rentas]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <td>
                        {{-- <a href="{{ route('rentaPendientes.edit', [$rentaPendiente->id_rentas]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a> --}}
                        <a href="{{ route('rentaPendientes.edit', [$rentaPendiente->id_rentas]) }}"
                            class='btn btn-default btn-xs btn-block'>Editar</a>
                    </td>
                    <td>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </td>

                        {!! Form::close() !!}

                </tr>
            @endforeach
        </tbody>
    </table>
</div>
