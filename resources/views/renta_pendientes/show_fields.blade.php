<!-- Grupo Field -->
<div class="form-group">
    {!! Form::label('grupo', 'Grupo:') !!}
    <p>{{ $rentaPendiente->grupo }}</p>
</div>

<!-- Proyecto Field -->
<div class="form-group">
    {!! Form::label('proyecto', 'Proyecto:') !!}
    <p>{{ $rentaPendiente->proyecto }}</p>
</div>

<!-- Id Cliente Field -->
<div class="form-group">
    {!! Form::label('id_cliente', 'Id Cliente:') !!}
    <p>{{ $rentaPendiente->id_cliente }}</p>
</div>

<!-- Estacionamiento Field -->
<div class="form-group">
    {!! Form::label('estacionamiento', 'Estacionamiento:') !!}
    <p>{{ $rentaPendiente->estacionamiento }}</p>
</div>

<!-- Concepto Field -->
<div class="form-group">
    {!! Form::label('concepto', 'Concepto:') !!}
    <p>{{ $rentaPendiente->concepto }}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('rfc', 'Rfc:') !!}
    <p>{{ $rentaPendiente->rfc }}</p>
</div>

<!-- Monto Cxp Field -->
<div class="form-group">
    {!! Form::label('monto_cxp', 'Monto Cxp:') !!}
    <p>{{ $rentaPendiente->monto_cxp }}</p>
</div>

<!-- Fecha Entrega Field -->
<div class="form-group">
    {!! Form::label('fecha_entrega', 'Fecha Entrega:') !!}
    <p>{{ $rentaPendiente->fecha_entrega }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $rentaPendiente->estatus }}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{{ $rentaPendiente->observaciones }}</p>
</div>

<!-- Gerente Field -->
<div class="form-group">
    {!! Form::label('gerente', 'Gerente:') !!}
    <p>{{ $rentaPendiente->gerente }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $rentaPendiente->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $rentaPendiente->updated_at }}</p>
</div>

