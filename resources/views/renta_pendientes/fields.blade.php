@livewire('rentas-pendientes', ['edit_id' => $edit_id])

<!-- Grupo Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('grupo', 'Grupo:') !!}
    {!! Form::text('grupo', null, ['class' => 'form-control', 'maxlength' => 55, 'maxlength' => 55, 'required']) !!}
</div> --}}

<!-- Concepto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('concepto', 'Concepto:') !!}
    {!! Form::text('concepto', null, ['class' => 'form-control', 'maxlength' => 255, 'maxlength' => 255, 'required']) !!}
</div>

<!-- Monto Cxp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_cxp', 'Monto Cxp:') !!}
    {!! Form::number('monto_cxp', null, ['class' => 'form-control', 'required' , 'step' => "0.01"]) !!}
</div>

<!-- Fecha Entrega Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_entrega', 'Fecha Entrega:') !!}
    {!! Form::text('fecha_entrega', null, ['class' => 'form-control', 'id' => 'fecha_entrega', 'required']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_entrega').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    <select name="estatus" class="form-control" required>
        <option value="En tiempo">En tiempo</option>
        <option value="Vencido">Vencido</option>
    </select>
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control', 'maxlength' => 1000, 'maxlength' => 1000, 'required']) !!}
</div>

<!-- Gerente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gerente', 'Gerente:') !!}
    <select name="gerente" class="form-control" required>
        @foreach ($gerentes as $gerente)
            <option value="{{$gerente->name}}">{{$gerente->name}}</option>
        @endforeach
    </select>
</div>

{{-- <!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::text('created_at', null, ['class' => 'form-control','id'=>'created_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::text('updated_at', null, ['class' => 'form-control','id'=>'updated_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('rentaPendientes.index') }}" class="btn btn-default">Cancelar</a>
</div>
