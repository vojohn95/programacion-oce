@if (Auth::user()->id_rol == '1')
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Usuarios</span></a>
    </li>

    <!--<li class="{{ Request::is('rols*') ? 'active' : '' }}">
    <a href="{{ route('rols.index') }}"><i class="fa fa-edit"></i><span>Rols</span></a>
</li>-->

    <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
        <a href="{{ route('clientes.index') }}"><i class="fa fa-edit"></i><span>Clientes</span></a>
    </li>

    <li class="{{ Request::is('promesas*') ? 'active' : '' }}">
        <a href="{{ route('promesas.index') }}"><i class="fa fa-edit"></i><span>Promesas</span></a>
    </li>

    <li class="{{ Request::is('historicos*') ? 'active' : '' }}">
        <a href="{{ route('historicos.index') }}"><i class="fa fa-edit"></i><span>Historicos</span></a>
    </li>

    <li class="{{ Request::is('saldos*') ? 'active' : '' }}">
        <a href="{{ route('saldos.index') }}"><i class="fa fa-edit"></i><span>Saldos</span></a>
    </li>

    <li class="{{ Request::is('ingresos*') ? 'active' : '' }}">
        <a href="{{ route('ingresos.index') }}"><i class="fa fa-edit"></i><span>Ingresos</span></a>
    </li>

    <li class="{{ Request::is('detalleCreditos*') ? 'active' : '' }}">
        <a href="{{ route('detalleCreditos.index') }}"><i class="fa fa-edit"></i><span>Detalle
                Creditos</span></a>
    </li>

    <li class="{{ Request::is('reportes*') ? 'active' : '' }}">
        <a href="{{ route('reportes.index') }}"><i class="fa fa-edit"></i><span>Reportes</span></a>
    </li>

    <li class="{{ Request::is('rentaPendientes*') ? 'active' : '' }}">
        <a href="{{ route('rentaPendientes.index') }}"><i class="fa fa-edit"></i><span>Renta
                Pendientes</span></a>
    </li>
@endif
@if (Auth::user()->id_rol == '3')
    {{-- <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
        <a href="{{ route('clientes.index') }}"><i class="fa fa-edit"></i><span>Clientes</span></a>
    </li> --}}

    <li class="{{ Request::is('promesas*') ? 'active' : '' }}">
        <a href="{{ route('promesas.index') }}"><i class="fa fa-edit"></i><span>Promesas</span></a>
    </li>

    <li class="{{ Request::is('reportes*') ? 'active' : '' }}">
        <a href="{{ route('reportes.index') }}"><i class="fa fa-edit"></i><span>Reportes</span></a>
    </li>
@endif
@if (Auth::user()->id_rol == '5')
     <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
        <a href="{{ route('clientes.index') }}"><i class="fa fa-edit"></i><span>Clientes</span></a>
    </li>

    <li class="{{ Request::is('promesas*') ? 'active' : '' }}">
        <a href="{{ route('promesas.index') }}"><i class="fa fa-edit"></i><span>Promesas</span></a>
    </li>

    <li class="{{ Request::is('reportes*') ? 'active' : '' }}">
        <a href="{{ route('reportes.index') }}"><i class="fa fa-edit"></i><span>Reportes</span></a>
    </li>
    <li class="{{ Request::is('rentaPendientes*') ? 'active' : '' }}">
        <a href="{{ route('rentaPendientes.index') }}"><i class="fa fa-edit"></i><span>Renta
                Pendientes</span></a>
    </li>
@endif

@if (Auth::user()->id_rol == '4')
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Usuarios</span></a>
    </li>

    <!--<li class="{{ Request::is('rols*') ? 'active' : '' }}">
    <a href="{{ route('rols.index') }}"><i class="fa fa-edit"></i><span>Rols</span></a>
</li>-->

    <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
        <a href="{{ route('clientes.index') }}"><i class="fa fa-edit"></i><span>Clientes</span></a>
    </li>

    <li class="{{ Request::is('promesas*') ? 'active' : '' }}">
        <a href="{{ route('promesas.index') }}"><i class="fa fa-edit"></i><span>Promesas</span></a>
    </li>

    <li class="{{ Request::is('saldos*') ? 'active' : '' }}">
        <a href="{{ route('saldos.index') }}"><i class="fa fa-edit"></i><span>Saldos</span></a>
    </li>

    <li class="{{ Request::is('ingresos*') ? 'active' : '' }}">
        <a href="{{ route('ingresos.index') }}"><i class="fa fa-edit"></i><span>Ingresos</span></a>
    </li>

    <li class="{{ Request::is('detalleCreditos*') ? 'active' : '' }}">
        <a href="{{ route('detalleCreditos.index') }}"><i class="fa fa-edit"></i><span>Detalle
                Creditos</span></a>
    </li>

    <li class="{{ Request::is('reportes*') ? 'active' : '' }}">
        <a href="{{ route('reportes.index') }}"><i class="fa fa-edit"></i><span>Reportes</span></a>
    </li>

    <li class="{{ Request::is('rentaPendientes*') ? 'active' : '' }}">
        <a href="{{ route('rentaPendientes.index') }}"><i class="fa fa-edit"></i><span>Renta
                Pendientes</span></a>
    </li>
@endif
