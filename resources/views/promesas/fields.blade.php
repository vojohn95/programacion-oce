<!-- Promesa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('promesa', 'Promesa:') !!}
    {!! Form::number('promesa', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Pagado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pagado', 'Nuevo pago:') !!}
    {!! Form::number('pagado', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <label for="exampleFormControlSelect1">Cliente:</label>
    <select class="form-control col-sm-6" name="id_cliente" id="exampleFormControlSelect1">
        @foreach($clientes as $cliente)
            <option value="{!! $cliente->id !!}">{!! $cliente->Cliente !!}</option>
        @endforeach
    </select>
</div>

<!-- Compromete Central Field -->
<div class="form-group col-sm-6">
    {!! Form::label('compromete_central', 'Compromete Central:') !!}
    {!! Form::text('compromete_central', null, ['class' => 'form-control' , 'required']) !!}
</div>

<!-- Compromete Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Compromete_cliente', 'Compromete Cliente:') !!}
    {!! Form::text('Compromete_cliente', null, ['class' => 'form-control' , 'required']) !!}
</div>

<!-- Fecha Promesa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Fecha_promesa', 'Fecha Promesa:') !!}
    {!! Form::text('Fecha_promesa', null, ['class' => 'form-control','id'=>'Fecha_promesa' , 'required']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#Fecha_promesa').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Fecha Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Fecha_pago', 'Fecha Pago:') !!}
    {!! Form::text('Fecha_pago', null, ['class' => 'form-control','id'=>'Fecha_pago' , 'required']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#Fecha_pago').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Ruta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ruta', 'Archivo:') !!}
    {!! Form::file('ruta', null, ['class' => 'form-control' , 'required']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('promesas.index') }}" class="btn btn-default">Cancelar</a>
</div>
