@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Detalle de pagos</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
                href="javascript:window.open('','_self').close();">Cerrar</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('layouts.errors')
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary py-2">
            <!-- Trigger the modal with a button -->
            @if (Auth::user()->id_rol != '3')
                @if (Auth::user()->id_rol != '5')
                    <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal">Cargar
                        Nuevo</button>
                @endif
            @endif
            <div class="box-body">
                @include('promesas.pagostable')
            </div>
        </div>
        <div class="text-center">
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Seleccione su nuevo documento a cargar</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['route' => 'comprobantes.store', 'enctype' => 'multipart/form-data']) !!}
                            {!! Form::hidden('id_promesas', $idPromesa) !!}

                            <!-- Fecha Promesa Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::label('Fecha_promesa', 'Fecha Promesa:') !!}
                                {!! Form::text('Fecha_promesa', null, ['class' => 'form-control', 'id' => 'Fecha_promesa', 'required']) !!}
                            </div>

                            <!-- Ruta Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::label('ruta', 'Archivo:') !!}
                                {!! Form::file('ruta', null, ['class' => 'form-control', 'required']) !!}
                            </div>

                            <div class="form-group col-sm-12">
                                <label for = "comentario">Comentario (opcional)</label>
                                <textarea class = "form-control" name="comentario" rows = "3" placeholder = "Comentario (opcional)"></textarea>
                            </div>

                            <!-- Submit Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                            </div>

                            {!! Form::close() !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#Fecha_promesa').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: true,
                sideBySide: true
            })
        </script>
    @endpush
@endsection
