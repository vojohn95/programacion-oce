<div class="table-responsive">
    <table class="table table-hover text-center" id="myTable">
        <thead>
            <tr>
                <th>ID</th>
                {{-- <th>Monto pago</th> --}}
                <th>Fecha pago</th>
                {{-- <th>Promesa</th>
                <th>Cliente</th> --}}
                <th>Documento</th>
                <th>Comentario</th>
                @if (Auth::user()->id_rol != '3')
                    @if (Auth::user()->id_rol != '5')
                        <th>Editar</th>
                        <th>Eliminar</th>
                    @endif
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($promesas as $promesa)
                <tr>
                    <td> {{ $promesa->idComprobante }}</td>
                    {{-- <td>$ {{ number_format($promesa->monto_pago, 2, '.', '') }}</td> --}}
                    <td>{{ $promesa->fecha_pago }}</td>
                    {{-- <td>{{ number_format($promesa->promesa, 2, '.', '') }}</td>
                    <td>{{ $promesa->cliente }}</td> --}}
                    <td>
                        @if ($promesa->documento == null)
                            No contiene archivo
                        @else
                            <a href="{!! route('pdf', [$promesa->idComprobante]) !!}" class='btn-floating btn-sm btn-blue-grey'>Descargar</a>
                        @endif
                    </td>
                    <td>
                        {{ $promesa->comentario }}
                    </td>
                    {{-- <td>
                    @if ($promesa->ruta == null)
                        No contiene archivo
                    @else
                        <a href="{!! route('pdf', [$promesa->idPromesas]) !!}"
                           class='btn-floating btn-sm btn-blue-grey'>Descargar</a>
                    @endif
                </td> --}}
                    @if (Auth::user()->id_rol != '3')
                        @if (Auth::user()->id_rol != '5')
                            {!! Form::open(['route' => ['comprobantes.destroy', $promesa->idComprobante], 'method' => 'delete']) !!}
                            <td><a href="{{ route('comprobantes.edit', [$promesa->idComprobante]) }}"
                                class='btn btn-default btn-xs btn-block'>Editar</a></td>
                            <td>{!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm ', 'onclick' => "return confirm('Are you sure?')"]) !!}</td>

                            {!! Form::close() !!}
                        @endif
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
