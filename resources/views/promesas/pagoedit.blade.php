@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pago
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($comprobante_pagos, ['route' => ['comprobantes.update', $comprobante_pagos->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                    <div class="modal-body">
                        {{ Form::hidden('id_promesas', $comprobante_pagos->id_promesas)}}
                        <!-- Fecha Promesa Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('Fecha_promesa', 'Fecha Promesa:') !!}
                            {!! Form::text('Fecha_promesa', $comprobante_pagos->fecha_pago, ['class' => 'form-control', 'id' => 'Fecha_promesa', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                        <p>
                            Archivo:
                            @if ($comprobante_pagos->documento == null)
                                No contiene archivo
                            @else
                                <a href="{!! route('pdf', [$comprobante_pagos->id]) !!}" class='btn-floating btn-sm btn-blue-grey'>Descargar</a>
                            @endif
                        </p>
                        </div>

                        <!-- Ruta Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('ruta', 'Archivo:') !!}
                            {!! Form::file('ruta', null, ['class' => 'form-control', 'required']) !!}
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="comentario">Comentario (opcional)</label>
                            {{ Form::textarea('comentario', $comprobante_pagos->comentario, [
                                'class' => 'form-control',
                                'rows' => 1,
                                'name' => 'comentario',
                                'id' => 'comentario',
                            ]) }}
                        </div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                            <a class="btn btn-danger" href="{{ url()->previous() }}">Cancelar</a>
                        </div>


                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
