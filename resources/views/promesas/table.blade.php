<div class="table-responsive">
    <table class="table text-center table-hover" id="myTable">
        <thead>
            <tr>
                <th>Promesa</th>
                <th>Pagado</th>
                <th>Restante</th>
                <th>Estatus</th>
                <th>Porcentaje comprometido</th>
                <th>Cliente</th>
                <th>Tipo cliente</th>
                <th>Compromete Central</th>
                <th>Compromete Cliente</th>
                <th>Fecha Promesa</th>
                {{-- <th>Fecha Pago</th> --}}
                <th>Archivos</th>
                @if (Auth::user()->id_rol != '3')
                    @if (Auth::user()->id_rol != '5')
                        <th>Editar</th>
                        <th>Eliminar</th>
                    @endif
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($promesas as $promesa)
                <tr>
                    <td>{{ number_format($promesa->promesa, 2, '.', '') }}</td>
                    <td>{{ number_format($promesa->pagado, 2, '.', '') }}</td>
                    <td>
                        {!! number_format($promesa->promesa - $promesa->pagado, 2, '.', '') !!}
                    </td>
                    <td>
                        <!--Se tiene que mover a la saldos y es desde el inicio de las fechas-->
                        @if (number_format($promesa->promesa, 2, '.', '') == number_format($promesa->pagado, 2, '.', ''))
                            <span class="text-success" style="font-size: 20px;">
                                <ion-icon name="checkmark-circle-outline"></ion-icon>
                            </span>
                        @else
                            <span class="text-danger" style="font-size: 20px;">
                                <ion-icon name="close-circle-outline"></ion-icon>
                            </span>
                        @endif
                    </td>
                    <td>
                        {!! $porcentaje = number_format(($promesa->pagado / $promesa->promesa) * 100, 2, '.', '') . '%' !!}
                    </td>
                    <td>{{ $promesa->cliente }}</td>
                    <td>{{ $promesa->Tipo }}</td>
                    <td>{{ $promesa->compromete_central }}</td>
                    <td>{{ $promesa->Compromete_cliente }}</td>
                    <td>{{ $promesa->Fecha_promesa }}</td>
                    {{-- <td>{{ $promesa->Fecha_pago }}</td> --}}
                    <td>
                        <a target="_blank" href="{!! route('pdfArchivos', [$promesa->idPromesas]) !!}"
                            class='btn-floating btn-sm btn-blue-grey'>Consultar</a>
                    </td>
                    {{-- <td>
                    @if ($promesa->ruta == null)
                        No contiene archivo
                    @else
                        <a href="{!! route('pdf', [$promesa->idPromesas]) !!}"
                           class='btn-floating btn-sm btn-blue-grey'>Descargar</a>
                    @endif
                </td> --}}
                    @if (Auth::user()->id_rol != '3')
                        @if (Auth::user()->id_rol != '5')
                            {!! Form::open(['route' => ['promesas.destroy', $promesa->idPromesas], 'method' => 'delete']) !!}
                            <!--<a href="{{ route('promesas.show', [$promesa->idPromesas]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                            {{-- @if ($porcentaje < 79) --}}
                            <td><a href="{{ route('promesas.edit', [$promesa->idPromesas]) }}"
                                    class='btn btn-default btn-xs btn-block'>Editar</a></td>
                            <td>{!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs btn-block', 'onclick' => "return confirm('Are you sure?')"]) !!}</td>
                            {{-- @else
                        <td></td>
                        <td>{!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm btn-block', 'onclick' => "return confirm('Are you sure?')"]) !!}</td>
                    @endif --}}
                            {!! Form::close() !!}
                        @endif
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
