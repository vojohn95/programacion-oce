<div>
    <!-- Id Cliente Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('id_cliente', ' Cliente:') !!}
        <select id="id_cliente" wire:model="cliente_id" name="id_cliente" class="form-control" required>
            <option value="">Seleccione un cliente</option>
            @foreach ($clientes as $cliente)
                <option value="{{ $cliente->id }}">{{ $cliente->Cliente }}</option>
            @endforeach
        </select>
    </div>

    <!-- Proyecto Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('proyecto', 'Proyecto:') !!}
        <input type="text" wire:model="no_est" class="form-control" name="proyecto" readonly>
    </div>

    <!-- Estacionamiento Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('estacionamiento', 'Estacionamiento:') !!}
        <input type="text" wire:model="cliente" class="form-control" name="estacionamiento" readonly>
    </div>

    <!-- Rfc Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('rfc', 'Rfc:') !!}
        <input type="text" wire:model="rfc" class="form-control" name="rfc" readonly>
    </div>

    <!-- Rfc Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('razon_social', 'Razón social:') !!}
        <input type="text" wire:model="razonS" class="form-control" name="razonS" readonly>
    </div>
</div>
