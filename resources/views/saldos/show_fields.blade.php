<!-- Monto Field -->
<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
    <p>{{ $saldo->monto }}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $saldo->fecha }}</p>
</div>

<!-- Autorizacion Field -->
<div class="form-group">
    {!! Form::label('autorizacion', 'Autorizacion:') !!}
    <p>{{ $saldo->autorizacion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $saldo->created_at }}</p>
</div>

<!-- Restante Field -->
<div class="form-group">
    {!! Form::label('restante', 'Restante:') !!}
    <p>{{ $saldo->restante }}</p>
</div>

