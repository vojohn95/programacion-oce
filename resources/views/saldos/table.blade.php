<div class="table-responsive">
    <table class="table table-hover text-center" id="saldos-table">
        <thead>
            <tr>
                <th>Monto</th>
        <th>Fecha</th>
        <th>Autorizacion</th>
        <th>Restante</th>
 <!--               <th colspan="3">Action</th>-->
            </tr>
        </thead>
        <tbody>
        @foreach($saldos as $saldo)
            <tr>
                <td>$ {{ number_format($saldo->monto, 2, '.', '') }}</td>
            <td>{{ $saldo->fecha->format('d/m/Y') }}</td>
            <td>{{ $saldo->autorizacion }}</td>
            <td>$ {{ number_format($saldo->restante, 2, '.', '') }}</td>
                <!--<td>
                    {!! Form::open(['route' => ['saldos.destroy', $saldo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('saldos.show', [$saldo->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('saldos.edit', [$saldo->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>-->
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<br>
<h3>Totales</h3>
<div class="table-responsive">
    <table class="table table-hover text-center" id="saldos-table">
        <thead>
        <tr>
            <th>Restante</th>
            <th>Pagado</th>
            <th>Porcentaje pagado</th>
        </tr>
        </thead>
        <tbody>
        @foreach($totales as $total)
            <tr>
                <td>$ {{ number_format($total->restante, 2, '.', '') }}</td>
                <td>$ {{ number_format($total->pagado, 2, '.', '') }}</td>
                <td>{{ number_format($total->porcentaje_pagado, 2, '.', '') ."%"}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
