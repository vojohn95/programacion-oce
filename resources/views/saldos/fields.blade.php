<!-- Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto', 'Monto:') !!}
    {!! Form::number('monto', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::text('fecha', null, ['class' => 'form-control','id'=>'fecha' , 'required']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Autorizacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('autorizacion', 'Autorizacion:') !!}
    {!! Form::text('autorizacion', null, ['class' => 'form-control' , 'required']) !!}
</div>

<!-- Created At Field -->




@push('scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Restante Field -->
<div class="form-group col-sm-6">
    {!! Form::label('restante', 'Restante:') !!}
    {!! Form::number('restante', null, ['class' => 'form-control' , 'required']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('saldos.index') }}" class="btn btn-default">Cancelar</a>
</div>
