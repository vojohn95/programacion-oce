<!-- Id Institucion Field -->
<div class="form-group">
    {!! Form::label('id_institucion', 'Institución:') !!}
    <p>{{ $detalleCredito->id_institucion }}</p>
</div>

<!-- No Contrato Field -->
<div class="form-group">
    {!! Form::label('no_contrato', 'No Contrato:') !!}
    <p>{{ $detalleCredito->no_contrato }}</p>
</div>

<!-- Monto Inicial Field -->
<div class="form-group">
    {!! Form::label('monto_inicial', 'Monto Inicial:') !!}
    <p>{{ $detalleCredito->monto_inicial }}</p>
</div>

<!-- Monto Pago Field -->
<div class="form-group">
    {!! Form::label('monto_pago', 'Monto Pago:') !!}
    <p>{{ $detalleCredito->monto_pago }}</p>
</div>

<!-- Monto Actual Field -->
<div class="form-group">
    {!! Form::label('monto_actual', 'Monto Actual:') !!}
    <p>{{ $detalleCredito->monto_actual }}</p>
</div>
