<!-- Id Institucion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_institucion', 'Institucion:') !!}
    <select name="id_institucion" id="id_institucion" class="form-control">
        <option value="">Seleccione una institución</option>
        @foreach ($clientes as $cliente)
            <option value="{{ $cliente->id }}">{{ $cliente->Cliente }}</option>
        @endforeach
    </select>
</div>

<!-- No Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_contrato', 'No Contrato:') !!}
    {!! Form::text('no_contrato', null, ['class' => 'form-control', 'maxlength' => 45, 'maxlength' => 45]) !!}
</div>

<!-- Monto Inicial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_inicial', 'Monto Inicial:') !!}
    {!! Form::number('monto_inicial', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_pago', 'Monto Pago:') !!}
    {!! Form::number('monto_pago', null, ['class' => 'form-control']) !!}
</div>

<!-- Comprobante Pago Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comprobante_pago', 'Comprobante Pago:') !!}
    <input type="file" name="comprobante_pago" class="form-control" accept="application/pdf" />
</div>

<!-- Monto Actual Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_actual', 'Monto Actual:') !!}
    {!! Form::number('monto_actual', null, ['class' => 'form-control']) !!}
</div>

<!-- Estatus Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    {!! Form::text('estatus', null, ['class' => 'form-control']) !!}
</div> --}}

@push('scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

@push('scripts')
    <script type="text/javascript">
        $('#deleted_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('detalleCreditos.index') }}" class="btn btn-default">Cancelar</a>
</div>
