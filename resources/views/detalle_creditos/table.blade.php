<div class="table-responsive">
    <table class="table" id="myTable">
        <thead>
            <tr>
                <th>Institución</th>
                <th>No Contrato</th>
                <th>Monto Inicial</th>
                <th>Monto Pago</th>
                <th>Comprobante Pago</th>
                <th>Monto Actual</th>
                <th>Estatus</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($detalleCreditos as $detalleCredito)
                <tr>
                    <td>{{ $detalleCredito->id_institucion }}</td>
                    <td>{{ $detalleCredito->no_contrato }}</td>
                    <td>{{ $detalleCredito->monto_inicial }}</td>
                    <td>{{ $detalleCredito->monto_pago }}</td>
                    <td>
                        @if ($detalleCredito->comprobante_pago == null)
                            No contiene archivo
                        @else
                            <a href="{!! route('pdfCreditos', [$detalleCredito->id]) !!}" class='btn-floating btn-sm btn-blue-grey'>Descargar</a>
                        @endif
                    </td>
                    <td>{{ $detalleCredito->monto_actual }}</td>
                    <td>{{ $detalleCredito->estatus }}</td>
                    <td>
                        {!! Form::open(['route' => ['detalleCreditos.destroy', $detalleCredito->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('detalleCreditos.show', [$detalleCredito->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{{ route('detalleCreditos.edit', [$detalleCredito->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
