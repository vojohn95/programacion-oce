@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Detalle Credito
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'detalleCreditos.store', 'enctype' => 'multipart/form-data']) !!}

                    @include('detalle_creditos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
