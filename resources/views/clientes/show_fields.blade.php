<!-- Cliente Field -->
<div class="form-group">
    {!! Form::label('Cliente', 'Cliente:') !!}
    <p>{{ $cliente->Cliente }}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('Tipo', 'Tipo:') !!}
    <p>{{ $cliente->Tipo }}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('Razon social', 'Razon social:') !!}
    @if($cliente->razon_social)
    <p>{{ $cliente->razon_social }}</p>
    @else
    <p>No se encontro razon social</p>
        @endif
</div>

<div class="form-group">
    {!! Form::label('RFC', 'RFC:') !!}
    @if($cliente->rfc)
        <p>{{ $cliente->rfc }}</p>
    @else
        <p>No se encontro rfc</p>
    @endif
</div>

<div class="form-group">
    {!! Form::label('no_estacionamiento', 'No. Estacionamiento:') !!}
    @if($cliente->no_estacionamiento)
        <p>{{ $cliente->no_estacionamiento }}</p>
    @else
        <p>No se encontro estacionamiento</p>
    @endif
</div>

