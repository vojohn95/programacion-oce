<div class="table-responsive">
    <table class="table table-hover text-center" id="myTable">
        <thead>
            <tr>
                <th>Cliente</th>
                <th>Tipo</th>
                <th>Ver</th>
                <th>Pagos</th>
                @if (Auth::user()->id_rol != '5')
                <th>Editar</th>
                <th>Eliminar</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($clientes as $cliente)
                <tr>
                    <td>{{ $cliente->Cliente }}</td>
                    <td>{{ $cliente->Tipo }}</td>
                    {!! Form::open(['route' => ['clientes.destroy', $cliente->id], 'method' => 'delete']) !!}
                    <td><a href="{{ route('clientes.show', [$cliente->id]) }}" class='btn btn-primary btn-xs'>Ver</a>
                    </td>
                    <td><a href="{!! route('historial-pagos', [$cliente->id]) !!}" class='btn btn-success btn-xs'>Pagos</a></td>
                    @if (Auth::user()->id_rol != '5')
                        <td><a href="{{ route('clientes.edit', [$cliente->id]) }}"
                                class='btn btn-default btn-xs'>Editar</a></td>
                        <td>{!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}</td>
                    @endif
                    {!! Form::close() !!}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
