<div class="table-responsive">
    <table class="table table-hover text-center" id="myTable">
        <thead>
        <tr>
            <th>Cliente</th>
            <th>Promesa</th>
            <th>Pagado</th>
            <th>Fecha pago</th>
            <th>Archivo</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $clie)
            <tr>
                <td>{{ $clie->cliente }}</td>
                <td>${{ number_format($clie->pagado, 2) }}</td>
                <td>${{ number_format($clie->pagado, 2) }}</td>
                <td>{{ $clie->Fecha_pago }}</td>
                <td>
                    @if($clie->ruta == null)
                        No contiene archivo
                    @else
                        <a href="{!! route('pdf', [$clie->idPromesas]) !!}"
                           class='btn-floating btn-sm btn-blue-grey'>Descargar</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@foreach($sumas as $suma)
    <div class="container">
        <div class="row">
            <div class="col">
                <h3>Total pagado: ${{number_format($suma->totalpagado, 2 )}}</h3>
            </div>
            <div class="col">
                <h3>Total promedio: ${{number_format($suma->totalprometido, 2 )}}</h3>
            </div>
        </div>
    </div>
@endforeach
