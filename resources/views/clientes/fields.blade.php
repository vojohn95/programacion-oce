<!-- Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Cliente', 'Cliente:') !!}
    {!! Form::text('Cliente', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Tipo', 'Tipo:') !!}
    <select name="Tipo" id="Tipo" class="form-control">
        <option value="">Seleccione un tipo</option>
        <option value="CLIENTE">CLIENTE</option>
        <option value="PROVEEDOR">PROVEEDOR</option>
        <option value="CREDITO">CRÉDITO</option>
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Añadir', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('clientes.index') }}" class="btn btn-default">Cancelar</a>
</div>
