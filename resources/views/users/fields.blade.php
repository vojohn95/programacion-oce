<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>


<!-- Id Rol Field -->
<div class="form-group col-sm-6">
    <label for="exampleFormControlSelect1">Rol:</label>
    <select class="form-control col-sm-6" name="id_rol" id="exampleFormControlSelect1">
        @foreach ($roles as $rol)
            @if ($rol->name != 'Developer')
                <option value="{!! $rol->id !!}">{!! $rol->name !!}</option>
            @endif
        @endforeach
    </select>
</div>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-default">Cancelar</a>
</div>
