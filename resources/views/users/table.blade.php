<div class="table-responsive">
    <table class="table table-hover text-center" id="users-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <!--<th>Email Verified At</th>-->
                <!--<th>Password</th>-->
                <!--<th>Remember Token</th>
        <th>Created At</th>
        <th>Updated At</th>-->
                <th>Rol</th>
                <th colspan="3">Accion</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if ($user->rol == 'Developer')
                            Administrador
                        @else
                            {{ $user->rol }}
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('users.edit', [$user->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                            {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
