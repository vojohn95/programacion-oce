<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $ingreso->fecha }}</p>
</div>

<!-- I Operacion Field -->
<div class="form-group">
    {!! Form::label('i_operacion', 'I Operacion:') !!}
    <p>{{ $ingreso->i_operacion }}</p>
</div>

<!-- I Cobranza Field -->
<div class="form-group">
    {!! Form::label('i_cobranza', 'I Cobranza:') !!}
    <p>{{ $ingreso->i_cobranza }}</p>
</div>

<!-- I Com Vta Equipo Field -->
<div class="form-group">
    {!! Form::label('i_com_vta_equipo', 'I Com Vta Equipo:') !!}
    <p>{{ $ingreso->i_com_vta_equipo }}</p>
</div>

<!-- E Remb Cliente Field -->
<div class="form-group">
    {!! Form::label('e_remb_cliente', 'E Remb Cliente:') !!}
    <p>{{ $ingreso->e_remb_cliente }}</p>
</div>

<!-- E Pago Prov Field -->
<div class="form-group">
    {!! Form::label('e_pago_prov', 'E Pago Prov:') !!}
    <p>{{ $ingreso->e_pago_prov }}</p>
</div>

<!-- E Morrallas Field -->
<div class="form-group">
    {!! Form::label('e_morrallas', 'E Morrallas:') !!}
    <p>{{ $ingreso->e_morrallas }}</p>
</div>

<!-- E Rem Caja Ch Field -->
<div class="form-group">
    {!! Form::label('e_rem_caja_ch', 'E Rem Caja Ch:') !!}
    <p>{{ $ingreso->e_rem_caja_ch }}</p>
</div>

<!-- E Nomina Field -->
<div class="form-group">
    {!! Form::label('e_nomina', 'E Nomina:') !!}
    <p>{{ $ingreso->e_nomina }}</p>
</div>

<!-- E Impuestos Field -->
<div class="form-group">
    {!! Form::label('e_impuestos', 'E Impuestos:') !!}
    <p>{{ $ingreso->e_impuestos }}</p>
</div>

<!-- P Pago Cred Field -->
<div class="form-group">
    {!! Form::label('p_pago_cred', 'P Pago Cred:') !!}
    <p>{{ $ingreso->p_pago_cred }}</p>
</div>

<!-- P Arrendamientos Field -->
<div class="form-group">
    {!! Form::label('p_arrendamientos', 'P Arrendamientos:') !!}
    <p>{{ $ingreso->p_arrendamientos }}</p>
</div>

<!-- P Comi Bancarias Field -->
<div class="form-group">
    {!! Form::label('p_comi_bancarias', 'P Comi Bancarias:') !!}
    <p>{{ $ingreso->p_comi_bancarias }}</p>
</div>

