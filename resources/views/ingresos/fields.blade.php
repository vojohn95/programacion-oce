<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::text('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- I Operacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('i_operacion', 'I Operacion:') !!}
    {!! Form::number('i_operacion', null, ['class' => 'form-control']) !!}
</div>

<!-- I Cobranza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('i_cobranza', 'I Cobranza:') !!}
    {!! Form::number('i_cobranza', null, ['class' => 'form-control']) !!}
</div>

<!-- I Com Vta Equipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('i_com_vta_equipo', 'I Com Vta Equipo:') !!}
    {!! Form::number('i_com_vta_equipo', null, ['class' => 'form-control']) !!}
</div>

<!-- E Remb Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_remb_cliente', 'E Remb Cliente:') !!}
    {!! Form::number('e_remb_cliente', null, ['class' => 'form-control']) !!}
</div>

<!-- E Pago Prov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_pago_prov', 'E Pago Prov:') !!}
    {!! Form::number('e_pago_prov', null, ['class' => 'form-control']) !!}
</div>

<!-- E Morrallas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_morrallas', 'E Morrallas:') !!}
    {!! Form::number('e_morrallas', null, ['class' => 'form-control']) !!}
</div>

<!-- E Rem Caja Ch Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_rem_caja_ch', 'E Rem Caja Ch:') !!}
    {!! Form::number('e_rem_caja_ch', null, ['class' => 'form-control']) !!}
</div>

<!-- E Nomina Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_nomina', 'E Nomina:') !!}
    {!! Form::number('e_nomina', null, ['class' => 'form-control']) !!}
</div>

<!-- E Impuestos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('e_impuestos', 'E Impuestos:') !!}
    {!! Form::number('e_impuestos', null, ['class' => 'form-control']) !!}
</div>

<!-- P Pago Cred Field -->
<div class="form-group col-sm-6">
    {!! Form::label('p_pago_cred', 'P Pago Cred:') !!}
    {!! Form::number('p_pago_cred', null, ['class' => 'form-control']) !!}
</div>

<!-- P Arrendamientos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('p_arrendamientos', 'P Arrendamientos:') !!}
    {!! Form::number('p_arrendamientos', null, ['class' => 'form-control']) !!}
</div>

<!-- P Comi Bancarias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('p_comi_bancarias', 'P Comi Bancarias:') !!}
    {!! Form::number('p_comi_bancarias', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ingresos.index') }}" class="btn btn-default">Cancelar</a>
</div>
