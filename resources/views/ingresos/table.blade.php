<div class="table-responsive">
    <table class="table table-hover table-responsive" style="width:auto;" id="ingresos-table">
        <thead style="width:auto;">
            <tr>
                <th>Fecha</th>
        <th>I Operacion</th>
        <th>I Cobranza</th>
        <th>I Com Vta Equipo</th>
        <th>E Remb Cliente</th>
        <th>E Pago Prov</th>
        <th>E Morrallas</th>
        <th>E Rem Caja Ch</th>
        <th>E Nomina</th>
        <th>E Impuestos</th>
        <th>P Pago Cred</th>
        <th>P Arrendamientos</th>
        <th>P Comi Bancarias</th>
                <th colspan="3">Accion</th>
            </tr>
        </thead>
        <tbody>
        @foreach($ingresos as $ingreso)
            <tr>
                <td>{{ $ingreso->fecha->format('d/m/Y') }}</td>
            <td>$ {{ number_format($ingreso->i_operacion, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->i_cobranza, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->i_com_vta_equipo, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->e_remb_cliente, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->e_pago_prov, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->e_morrallas, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->e_rem_caja_ch, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->e_nomina, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->e_impuestos, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->p_pago_cred, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->p_arrendamientos, 2, '.', '') }}</td>
            <td>$ {{ number_format($ingreso->p_comi_bancarias, 2, '.', '') }}</td>
                <td>
                    {!! Form::open(['route' => ['ingresos.destroy', $ingreso->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{{ route('ingresos.show', [$ingreso->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                        <a href="{{ route('ingresos.edit', [$ingreso->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
