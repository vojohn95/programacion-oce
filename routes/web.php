<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::Routes();
Route::group(['middleware' => 'auth'],function (){
    Route::get('/', 'PromesaController@index')->middleware('verified');
    Route::resource('users', 'UserController');
    Route::resource('rols', 'RolController');
    Route::resource('clientes', 'ClienteController');
    Route::resource('promesas', 'PromesaController');
    Route::resource('saldos', 'SaldoController');
    Route::resource('ingresos', 'IngresoController');
    Route::get('/pdf/{id}', 'PromesaController@downloadPDF')->name('pdf');
    Route::get('/historial-pagos/{id}', 'ClienteController@pagos')->name('historial-pagos');
    Route::resource('historicos', 'HistoricoController');
    Route::resource('detalleCreditos', 'DetalleCreditoController');
    Route::get('/pdfCreditos/{id}', 'DetalleCreditoController@downloadPDF')->name('pdfCreditos');
    Route::resource('reportes', 'ReporteController');
    Route::post('reportesProm/export/', 'PromesaController@export')->name('reportesProm');
    Route::post('reportesProm/exportCliente/', 'PromesaController@exportCliente')->name('reportesPromCliente');
    Route::get('/pdfArchivos/{id}', 'PromesaController@retreivePagos')->name('pdfArchivos');
    Route::resource('comprobantes', 'CargaComprobantesController');
    Route::resource('rentaPendientes', 'RentaPendienteController');

});
