<?php

namespace App\Exports;

use App\Models\Promesa;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ClientesExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(string $inicio, string $fin)
    {
        $this->inicio = $inicio;
        $this->fin = $fin;
    }

    public function array(): array
    {
        if (Auth::user()->id_rol != 3) {
            $promesas = "select * from u548444544_promesas.total_pagos_cliente_v WHERE (fecha_pago BETWEEN '$this->inicio' AND '$this->fin')";
        }else {
            if ($this->inicio <= '2021-11-01') {
                $promesas = "select * from u548444544_promesas.total_pagos_cliente_v WHERE (fecha_pago BETWEEN '2021-11-01' AND '$this->fin')";
            }else {
                $promesas = "select * from u548444544_promesas.total_pagos_cliente_v WHERE (fecha_pago BETWEEN '$this->inicio' AND '$this->fin')";
            }

        }
        $result1 = \Illuminate\Support\Facades\DB::SELECT($promesas);
        //dd($result1);
        return $result1;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Cliente',
            'RFC',
            'Monto Pagado',
            'Número de estacionamiento',
            'Fecha Pago',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
