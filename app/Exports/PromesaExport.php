<?php

namespace App\Exports;

use App\Models\Promesa;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PromesaExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public function __construct(string $inicio, string $fin)
    {
        $this->inicio = $inicio;
        $this->fin = $fin;
    }

    public function collection()
    {
        /*$promesa = Promesa::whereBetween('Fecha_promesa', [$this->inicio, $this->fin])->select('idPromesas', 'promesa', 'pagado', 'id_cliente', 'Fecha_promesa', 'Fecha_pago')->with(['idCliente' => function($query){
            $query->select('id', 'Cliente', 'Tipo');
        }])->get();*/
        $promesa = Promesa::select('Promesas.idPromesas', 'Promesas.promesa', 'Promesas.pagado', 'Clientes.Cliente','Promesas.Fecha_promesa', 'Promesas.Fecha_pago')
            ->join('Clientes', 'Clientes.id', '=', 'Promesas.id_cliente')
            ->whereBetween('Fecha_promesa', [$this->inicio, $this->fin])
            ->get();
        //dd($promesa);
        return $promesa;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Promesa',
            'Pagado',
            'Cliente',
            'Fecha Promesa',
            'Fecha Pago',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
