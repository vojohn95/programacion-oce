<?php

namespace App\Http\Controllers;

use DB;
use Flash;
use Response;
use Carbon\Carbon;
use App\Models\Cliente;
use App\Models\Promesa;
use Illuminate\Http\Request;
use App\Exports\PromesaExport;
use App\Exports\ClientesExport;
use App\Models\ComprobantesPago;

use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\PromesaRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePromesaRequest;
use App\Http\Requests\UpdatePromesaRequest;
use Illuminate\Support\Facades\Auth;

class PromesaController extends AppBaseController
{
    /** @var  PromesaRepository */
    private $promesaRepository;

    public function __construct(PromesaRepository $promesaRepo)
    {
        $this->promesaRepository = $promesaRepo;
    }

    /**
     * Display a listing of the Promesa.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //$promesas = $this->promesaRepository->all();
        $now = new \DateTime('Y');
        /*$promesas = DB::table('Promesas')
            ->join('Clientes', 'Promesas.id_cliente', '=', 'Clientes.id')
            ->select('Promesas.*', 'Clientes.cliente as cliente')
            ->where('Fecha_promesa', '=', '2020-10-02')
            ->paginate(50);*/
        /*$promesas = "select Promesas.idPromesas, Promesas.promesa, comprobantes_pagos.monto_pago as pagado, Promesas.id_cliente, Promesas.compromete_central, Promesas.Compromete_cliente,
        Promesas.Fecha_promesa, comprobantes_pagos.created_at as Fecha_pago, comprobantes_pagos.documento as ruta, Promesas.created_at, Promesas.updated_at,
         Clientes.cliente, Clientes.Tipo from Promesas inner join comprobantes_pagos on (comprobantes_pagos.id_promesas = Promesas.idPromesas)
         inner join Clientes on (Promesas.id_cliente = Clientes.id)
                where date_format(Promesas.fecha_promesa, '%Y') = date_format(sysdate(), '%Y');";*/
        /*$promesas = "select Promesas.*, Clientes.cliente, Clientes.Tipo from Promesas inner join Clientes on (Promesas.id_cliente = Clientes.id)
        where date_format(Promesas.fecha_promesa, '%Y') = date_format(sysdate(), '%Y');";*/
        if (Auth::user()->id_rol != 3) {
            $promesas = "select Promesas.idPromesas, Promesas.promesa, sum(comprobantes_pagos.monto_pago) as pagado, Promesas.id_cliente, Promesas.compromete_central, Promesas.Compromete_cliente,
        Promesas.Fecha_promesa, Promesas.created_at, Promesas.updated_at,
         Clientes.cliente, Clientes.Tipo from Promesas
         left join comprobantes_pagos on (comprobantes_pagos.id_promesas = Promesas.idPromesas)
         inner join Clientes on (Promesas.id_cliente = Clientes.id)
		where date_format(Promesas.fecha_promesa, '%Y') = date_format(sysdate(), '%Y')
		 group by Promesas.idPromesas
                order by Fecha_promesa desc;";
        } else {
            $promesas = "select Promesas.idPromesas, Promesas.promesa, sum(comprobantes_pagos.monto_pago) as pagado, Promesas.id_cliente, Promesas.compromete_central, Promesas.Compromete_cliente,
        Promesas.Fecha_promesa, Promesas.created_at, Promesas.updated_at,
         Clientes.cliente, Clientes.Tipo from Promesas
         left join comprobantes_pagos on (comprobantes_pagos.id_promesas = Promesas.idPromesas)
         inner join Clientes on (Promesas.id_cliente = Clientes.id)
		where date_format(Promesas.fecha_promesa, '%Y') = date_format(sysdate(), '%Y')
        and date_format(Promesas.fecha_promesa, '%Y-%m-%d') > '2021-11-01'
		 group by Promesas.idPromesas
                order by Fecha_promesa desc;";
        }

        $result1 = \Illuminate\Support\Facades\DB::SELECT($promesas);
        //$result11 = \Illuminate\Support\Facades\DB::SELECT($promesas1);
        //dd($result1);

        return view('promesas.index')
            ->with('promesas', $result1);
    }

    /**
     * Show the form for creating a new Promesa.
     *
     * @return Response
     */
    public function create()
    {
        $cliente = Cliente::all();
        return view('promesas.create')->with('clientes', $cliente);
    }

    /**
     * Store a newly created Promesa in storage.
     *
     * @param CreatePromesaRequest $request
     *
     * @return Response
     */
    public function store(CreatePromesaRequest $request)
    {
        $input = $request->all();
        //$file = $request->file('ruta');

        if ($file = $request->file('ruta')) {
            $image = base64_encode(file_get_contents($request->file('ruta')));
            $input['ruta'] = $image;
        } else {
            $input['ruta'] = null;
        }

        $promesa = $this->promesaRepository->create($input);
        /*if ($file = $request->file('ruta')) {
            $image = base64_encode(file_get_contents($request->file('ruta')));
            $input['ruta'] = $image;
        } else {
            $input['ruta'] = null;
        }*/
        //dd($promesa);

        $comprobante_pago = new ComprobantesPago;
        $comprobante_pago->id_promesas = $promesa->id;
        $comprobante_pago->documento = $input['ruta'];
        $comprobante_pago->fecha_pago = $input['Fecha_pago'];
        $comprobante_pago->monto_pago = $input['pagado'];
        $comprobante_pago->save();
        //dd($comprobante_pago);
        Flash::success('Promesa añadida.');

        return redirect(route('promesas.index'));
    }

    /**
     * Display the specified Promesa.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $promesa = $this->promesaRepository->find($id);

        if (empty($promesa)) {
            Flash::error('Promesa not found');

            return redirect(route('promesas.index'));
        }

        return view('promesas.show')->with('promesa', $promesa);
    }

    /**
     * Show the form for editing the specified Promesa.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $query = DB::table('Promesas')->where('idPromesas', '=', $id);
        $promesa = $query->first();
        $cliente = Cliente::all();
        //dd($promesa);

        if (empty($promesa)) {
            Flash::error('Promesa not found');

            return redirect(route('promesas.index'));
        }

        return view('promesas.edit')->with('promesa', $promesa)->with('clientes', $cliente);
    }

    /**
     * Update the specified Promesa in storage.
     *
     * @param int $id
     * @param UpdatePromesaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromesaRequest $request)
    {
        $input = $request->all();
        $query = DB::table('Promesas')->where('idPromesas', '=', $id);
        $promesa = $query->first();
        $file = $request->file('ruta');
        //dd($file);
        //dd($input);

        if (empty($promesa)) {
            Flash::error('Promesa not found');

            return redirect(route('promesas.index'));
        }

        $update = $query
            ->update([
                'promesa' => $input['promesa'],
                //'pagado' => $input['pagado'],
                'id_cliente' => $input['id_cliente'],
                'compromete_central' => $input['compromete_central'],
                'Compromete_cliente' => $input['Compromete_cliente'],
                'Fecha_promesa' => $input['Fecha_promesa'],
                'Fecha_pago' => $input['Fecha_pago'],
            ]);

        if ($file == null) {
            $up = $query
                ->update(['ruta' => null]);
            $image = null;
        } else {
            $image = base64_encode(file_get_contents($request->file('ruta')));
            $up = $query
                ->update(['ruta' => $image]);
        }

        $comprobante_pago = new ComprobantesPago;
        $comprobante_pago->id_promesas = $id;
        $comprobante_pago->documento = $image;
        $comprobante_pago->fecha_pago = $input['Fecha_pago'];
        $comprobante_pago->monto_pago = $input['pagado'];
        $comprobante_pago->save();


        Flash::success('Promesa actualizada.');

        return redirect(route('promesas.index'));
    }

    /**
     * Remove the specified Promesa from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        //$promesa = $this->promesaRepository->find($id);
        $query = DB::table('Promesas')->where('idPromesas', '=', $id);
        $promesa = $query->first();
        //dd($promesa);

        if (empty($promesa)) {
            Flash::error('Promesa not found');

            return redirect(route('promesas.index'));
        }

        $query->delete();

        Flash::success('Promesa eliminada.');

        return redirect(route('promesas.index'));
    }

    public function downloadPDF($id)
    {
        //$id = \request()->request;
        $query = ComprobantesPago::find($id);
        //$query = DB::table('Promesas')->where('idPromesas', '=', $id);
        //$pdf = $query->first();
        //dd($pdf);
        $archivo = $query->documento;
        $uuid = $query->id_promesas . "-" . $id;

        $decoded = base64_decode($archivo);
        $path = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=$uuid.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

    public function export()
    {
        $input = request()->all();
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();
        $date = $date->format('d-m-Y');

        $file_name = "Reporte-" . $date . ".xlsx";
        return (new PromesaExport($input['inicio'], $input['fin']))->download($file_name);
        //return Excel::download(new PromesaExport, 'promesa.xlsx');
    }

    public function exportCliente()
    {
        $input = request()->all();
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();
        $date = $date->format('d-m-Y');

        $file_name = "Reporte-" . $date . ".xlsx";
        return (new ClientesExport($input['inicio'], $input['fin']))->download($file_name);
        //return Excel::download(new PromesaExport, 'promesa.xlsx');
    }

    public function retreivePagos($id)
    {
        $promesas = "select Promesas.idPromesas, Promesas.id_cliente, Clientes.cliente, Promesas.promesa, comprobantes_pagos.monto_pago, comprobantes_pagos.id as idComprobante, comprobantes_pagos.comentario,
        comprobantes_pagos.fecha_pago as fecha_pago, comprobantes_pagos.documento
        from Promesas
        inner join comprobantes_pagos on (comprobantes_pagos.id_promesas = Promesas.idPromesas)
        inner join Clientes on (Promesas.id_cliente = Clientes.id)
               where id_promesas = '$id' #pasar el id de la promesa aqui
               #and (monto_pago > 0 && monto_pago is not null)
               order by fecha_pago asc;";

        $result1 = \Illuminate\Support\Facades\DB::SELECT($promesas);
        //dd($id);

        return view('promesas.pagos')
            ->with('promesas', $result1)->with('idPromesa', $id);
    }
}
