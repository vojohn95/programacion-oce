<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSaldoRequest;
use App\Http\Requests\UpdateSaldoRequest;
use App\Repositories\SaldoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;

class SaldoController extends AppBaseController
{
    /** @var  SaldoRepository */
    private $saldoRepository;

    public function __construct(SaldoRepository $saldoRepo)
    {
        $this->saldoRepository = $saldoRepo;
    }

    /**
     * Display a listing of the Saldo.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $date = date('Y-m-d');
  //      dd($date);
        $saldos = $this->saldoRepository->all();
        $totales = "select (sl.monto - sum(pr.pagado)) restante,
        (sum(pr.pagado)) pagado,
        ((sum(pr.pagado) * 100) / sl.monto ) porcentaje_pagado
        from saldos sl, Promesas pr where pr.Fecha_pago = '$date' and sl.fecha = pr.Fecha_pago
        group by pr.Fecha_promesa, sl.fecha, sl.monto";

        $result1 = DB::SELECT($totales);
        //dd($result1);

        return view('saldos.index')
            ->with('saldos', $saldos)->with('totales', $result1);
    }

    /**
     * Show the form for creating a new Saldo.
     *
     * @return Response
     */
    public function create()
    {
        return view('saldos.create');
    }

    /**
     * Store a newly created Saldo in storage.
     *
     * @param CreateSaldoRequest $request
     *
     * @return Response
     */
    public function store(CreateSaldoRequest $request)
    {
        $input = $request->all();

        $saldo = $this->saldoRepository->create($input);

        Flash::success('Saldo añadido.');

        return redirect(route('saldos.index'));
    }

    /**
     * Display the specified Saldo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $saldo = $this->saldoRepository->find($id);

        if (empty($saldo)) {
            Flash::error('Saldo not found');

            return redirect(route('saldos.index'));
        }

        return view('saldos.show')->with('saldo', $saldo);
    }

    /**
     * Show the form for editing the specified Saldo.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $saldo = $this->saldoRepository->find($id);

        if (empty($saldo)) {
            Flash::error('Saldo not found');

            return redirect(route('saldos.index'));
        }

        return view('saldos.edit')->with('saldo', $saldo);
    }

    /**
     * Update the specified Saldo in storage.
     *
     * @param int $id
     * @param UpdateSaldoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSaldoRequest $request)
    {
        $saldo = $this->saldoRepository->find($id);

        if (empty($saldo)) {
            Flash::error('Saldo not found');

            return redirect(route('saldos.index'));
        }

        $saldo = $this->saldoRepository->update($request->all(), $id);

        Flash::success('Saldo actualizado.');

        return redirect(route('saldos.index'));
    }

    /**
     * Remove the specified Saldo from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $saldo = $this->saldoRepository->find($id);

        if (empty($saldo)) {
            Flash::error('Saldo not found');

            return redirect(route('saldos.index'));
        }

        $this->saldoRepository->delete($id);

        Flash::success('Saldo eliminado.');

        return redirect(route('saldos.index'));
    }
}
