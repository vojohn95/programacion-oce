<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DetalleCreditoRepository;
use App\Http\Requests\CreateDetalleCreditoRequest;
use App\Http\Requests\UpdateDetalleCreditoRequest;

class DetalleCreditoController extends AppBaseController
{
    /** @var  DetalleCreditoRepository */
    private $detalleCreditoRepository;

    public function __construct(DetalleCreditoRepository $detalleCreditoRepo)
    {
        $this->detalleCreditoRepository = $detalleCreditoRepo;
    }

    /**
     * Display a listing of the DetalleCredito.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $detalleCreditos = $this->detalleCreditoRepository->all();

        return view('detalle_creditos.index')
            ->with('detalleCreditos', $detalleCreditos);
    }

    /**
     * Show the form for creating a new DetalleCredito.
     *
     * @return Response
     */
    public function create()
    {

        $clientes = Cliente::select('*')->where('Tipo', '=', 'CREDITO')->get();

        return view('detalle_creditos.create')->with('clientes', $clientes);
    }

    /**
     * Store a newly created DetalleCredito in storage.
     *
     * @param CreateDetalleCreditoRequest $request
     *
     * @return Response
     */
    public function store(CreateDetalleCreditoRequest $request)
    {
        $input = $request->all();
        //$file = $request->file('ruta');
        //dd($file);
        if ($file = $request->file('comprobante_pago')) {
            $image = base64_encode(file_get_contents($request->file('comprobante_pago')));
            $input['comprobante_pago'] = $image;
        } else {
            $input['comprobante_pago'] = null;
        }

        $detalleCredito = $this->detalleCreditoRepository->create($input);

        Flash::success('Detalle añadido satisfactoriamente.');

        return redirect(route('detalleCreditos.index'));
    }

    /**
     * Display the specified DetalleCredito.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detalleCredito = $this->detalleCreditoRepository->find($id);

        if (empty($detalleCredito)) {
            Flash::error('Detalle Credito not found');

            return redirect(route('detalleCreditos.index'));
        }

        return view('detalle_creditos.show')->with('detalleCredito', $detalleCredito);
    }

    /**
     * Show the form for editing the specified DetalleCredito.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detalleCredito = $this->detalleCreditoRepository->find($id);

        if (empty($detalleCredito)) {
            Flash::error('Detalle Credito not found');

            return redirect(route('detalleCreditos.index'));
        }

        $clientes = Cliente::select('*')->where('Tipo', '=', 'CREDITO')->get();

        return view('detalle_creditos.edit')->with('detalleCredito', $detalleCredito)->with('clientes', $clientes);
    }

    /**
     * Update the specified DetalleCredito in storage.
     *
     * @param int $id
     * @param UpdateDetalleCreditoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetalleCreditoRequest $request)
    {
        //$detalleCredito = $this->detalleCreditoRepository->find($id);
        $input = $request->all();
        $query = DB::table('detalle_credito')->where('id', '=', $id);
        $detalleCredito = $query->first();
        $file = $request->file('comprobante_pago');

        if (empty($detalleCredito)) {
            Flash::error('Detalle Credito not found');

            return redirect(route('detalleCreditos.index'));
        }

        $update = $query
            ->update([
                'id_institucion' => $input['id_institucion'],
                'no_contrato' => $input['no_contrato'],
                'monto_inicial' => $input['monto_inicial'],
                'monto_pago' => $input['monto_pago'],
                'monto_actual' => $input['monto_actual'],
            ]);

        if ($file == null) {
            $up = $query
                ->update(['comprobante_pago' => null]);
        } else {
            $image = base64_encode(file_get_contents($request->file('comprobante_pago')));
            $up = $query
                ->update(['comprobante_pago' => $image]);
        }

        //$detalleCredito = $this->detalleCreditoRepository->update($request->all(), $id);

        Flash::success('Detalle actualizada.');

        return redirect(route('detalleCreditos.index'));
    }

    /**
     * Remove the specified DetalleCredito from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detalleCredito = $this->detalleCreditoRepository->find($id);

        if (empty($detalleCredito)) {
            Flash::error('Detalle Credito not found');

            return redirect(route('detalleCreditos.index'));
        }

        $this->detalleCreditoRepository->delete($id);

        Flash::success('Detalle eliminado satisfactoriamente.');

        return redirect(route('detalleCreditos.index'));
    }

    public function downloadPDF($id)
    {
        //$id = \request()->request;
        //dd($id);
        $query = DB::table('detalle_credito')->where('id', '=', $id);
        $pdf = $query->first();
        //dd($pdf);
        $archivo = $pdf->comprobante_pago;
        $uuid = $pdf->id . $pdf->no_contrato;

        $decoded = base64_decode($archivo);
        $path = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=$uuid.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }
}
