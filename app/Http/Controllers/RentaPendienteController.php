<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRentaPendienteRequest;
use App\Http\Requests\UpdateRentaPendienteRequest;
use App\Repositories\RentaPendienteRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Gerente;
use Illuminate\Http\Request;
use Flash;
use Response;

class RentaPendienteController extends AppBaseController
{
    /** @var  RentaPendienteRepository */
    private $rentaPendienteRepository;

    public function __construct(RentaPendienteRepository $rentaPendienteRepo)
    {
        $this->rentaPendienteRepository = $rentaPendienteRepo;
    }

    /**
     * Display a listing of the RentaPendiente.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $rentaPendientes = $this->rentaPendienteRepository->all();

        return view('renta_pendientes.index')
            ->with('rentaPendientes', $rentaPendientes);
    }

    /**
     * Show the form for creating a new RentaPendiente.
     *
     * @return Response
     */
    public function create()
    {
        $edit_id = false;
        $gerentes = Gerente::select('name')->distinct()->get();

        return view('renta_pendientes.create', compact('edit_id', 'gerentes'));
    }

    /**
     * Store a newly created RentaPendiente in storage.
     *
     * @param CreateRentaPendienteRequest $request
     *
     * @return Response
     */
    public function store(CreateRentaPendienteRequest $request)
    {
        $input = $request->all();

        $rentaPendiente = $this->rentaPendienteRepository->create($input);

        Flash::success('Renta añadida satisfactoriamente.');

        return redirect(route('rentaPendientes.index'));
    }

    /**
     * Display the specified RentaPendiente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rentaPendiente = $this->rentaPendienteRepository->find($id);

        if (empty($rentaPendiente)) {
            Flash::error('Renta Pendiente not found');

            return redirect(route('rentaPendientes.index'));
        }

        return view('renta_pendientes.show')->with('rentaPendiente', $rentaPendiente);
    }

    /**
     * Show the form for editing the specified RentaPendiente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $rentaPendiente = $this->rentaPendienteRepository->find($id);

        if (empty($rentaPendiente)) {
            Flash::error('Renta Pendiente not found');

            return redirect(route('rentaPendientes.index'));
        }

        $gerentes = Gerente::select('name')->distinct()->get();
        return view('renta_pendientes.edit', compact('gerentes'))->with('rentaPendiente', $rentaPendiente)->with('edit_id', $rentaPendiente->id_rentas);
    }

    /**
     * Update the specified RentaPendiente in storage.
     *
     * @param int $id
     * @param UpdateRentaPendienteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRentaPendienteRequest $request)
    {
        $rentaPendiente = $this->rentaPendienteRepository->find($id);

        if (empty($rentaPendiente)) {
            Flash::error('Renta Pendiente not found');

            return redirect(route('rentaPendientes.index'));
        }

        $rentaPendiente = $this->rentaPendienteRepository->update($request->all(), $id);

        Flash::success('Renta actualizada.');

        return redirect(route('rentaPendientes.index'));
    }

    /**
     * Remove the specified RentaPendiente from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rentaPendiente = $this->rentaPendienteRepository->find($id);

        if (empty($rentaPendiente)) {
            Flash::error('Renta Pendiente not found');

            return redirect(route('rentaPendientes.index'));
        }

        $this->rentaPendienteRepository->delete($id);

        Flash::success('Renta eliminada.');

        return redirect(route('rentaPendientes.index'));
    }
}
