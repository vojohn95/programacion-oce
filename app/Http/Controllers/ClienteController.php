<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClienteRequest;
use App\Http\Requests\UpdateClienteRequest;
use App\Repositories\ClienteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;

class ClienteController extends AppBaseController
{
    /** @var  ClienteRepository */
    private $clienteRepository;

    public function __construct(ClienteRepository $clienteRepo)
    {
        $this->clienteRepository = $clienteRepo;
    }

    /**
     * Display a listing of the Cliente.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $clientes = $this->clienteRepository->all();

        return view('clientes.index')
            ->with('clientes', $clientes);
    }

    /**
     * Show the form for creating a new Cliente.
     *
     * @return Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created Cliente in storage.
     *
     * @param CreateClienteRequest $request
     *
     * @return Response
     */
    public function store(CreateClienteRequest $request)
    {
        $input = $request->all();

        if (is_null($input['Tipo'])) {
            Flash::error('Seleccione un tipo');

            return redirect(route('clientes.create'));
        }

        $cliente = $this->clienteRepository->create($input);

        Flash::success('Cliente añadido.');

        return redirect(route('clientes.index'));
    }

    /**
     * Display the specified Cliente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cliente = $this->clienteRepository->find($id);

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        return view('clientes.show')->with('cliente', $cliente);
    }

    public function pagos($id)
    {
        $cliente = $this->clienteRepository->find($id);

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        $totales = "select cl.cliente, pr.idPromesas ,pr.promesa, pr.ruta ,pr.pagado, pr.Fecha_pago  from u548444544_promesas.Clientes cl
        inner join u548444544_promesas.Promesas pr
        on cl.id = pr.id_cliente where id_cliente = $id";

        $sumas = "select cl.cliente, sum(pr.promesa) as totalprometido, SUM(pagado) as totalpagado
        from u548444544_promesas.Clientes cl
        inner join u548444544_promesas.Promesas pr
        on cl.id = pr.id_cliente where id_cliente = $id
        group by cl.cliente";

        $result1 = DB::SELECT($totales);
        $result2 = DB::SELECT($sumas);
        //dd($result2);
        //$cliente = $result1[0]->cliente;

        return view('clientes.pagos')->with('clients', $result1)->with('sumas', $result2);
    }

    /**
     * Show the form for editing the specified Cliente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cliente = $this->clienteRepository->find($id);

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        return view('clientes.edit')->with('cliente', $cliente);
    }

    /**
     * Update the specified Cliente in storage.
     *
     * @param int $id
     * @param UpdateClienteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClienteRequest $request)
    {
        $cliente = $this->clienteRepository->find($id);

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        $input = request()->all();

        if (!is_null($input['Tipo'])) {
            $cliente = $this->clienteRepository->update($request->all(), $id);
        } else {
            $cliente->Cliente = $input['Tipo'];
        }

        Flash::success('Cliente actualizado.');

        return redirect(route('clientes.index'));
    }

    /**
     * Remove the specified Cliente from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $cliente = $this->clienteRepository->find($id);

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        $this->clienteRepository->delete($id);

        Flash::success('Cliente eliminado.');

        return redirect(route('clientes.index'));
    }
}
