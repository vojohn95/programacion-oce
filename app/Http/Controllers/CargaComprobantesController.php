<?php

namespace App\Http\Controllers;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use App\Models\ComprobantesPago;
use Illuminate\Support\Facades\DB;

class CargaComprobantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($file = $request->file('ruta')) {
            $image = base64_encode(file_get_contents($request->file('ruta')));
            $input['ruta'] = $image;
        } else {
            $input['ruta'] = null;
        }
        $comprobante_pago = new ComprobantesPago;
        $comprobante_pago->id_promesas = $request->id_promesas;
        //$comprobante_pago->monto_pago = $request->monto;
        $comprobante_pago->fecha_pago = $request->Fecha_promesa;
        $comprobante_pago->comentario = $request->comentario;
        $comprobante_pago->documento = $input['ruta'];
        $comprobante_pago->save();
        //dd($comprobante_pago);

        Flash::success('Documento añadido.');

        return redirect(route('pdfArchivos', [$request->id_promesas]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comprobante_pagos = ComprobantesPago::find($id);

        return view('promesas.pagoedit', compact('comprobante_pagos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comprobante = ComprobantesPago::find($id);
        if ($file = $request->file('ruta')) {
            $image = base64_encode(file_get_contents($request->file('ruta')));
            $input['ruta'] = $image;
            $comprobante->documento = $input['ruta'];
        } else {
            $input['ruta'] = null;
        }

        $comprobante->id_promesas = $request->id_promesas;
        //$comprobante->monto_pago = $request->monto;
        $comprobante->fecha_pago = $request->Fecha_promesa;
        $comprobante->comentario = $request->comentario;
        $comprobante->save();
        //dd($comprobante);
        Flash::success('Documento añadido.');

        return redirect(route('pdfArchivos', [$request->id_promesas]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('comprobantes_pagos')->select('id')->where('id', '=', $id)->get();

        if (empty($query)) {
            Flash::error('Comprobante not found');

            return redirect()->back();
        }

        DB::table('comprobantes_pagos')->select('id')->where('id', '=', $id)->delete();

        Flash::success('Comprobante eliminado.');

        return redirect()->back();
    }
}
