<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Cliente;
use App\Models\RentaPendiente;

class RentasPendientes extends Component
{

    public $clientes, $cliente, $cliente_id, $estacionamiento, $rfc, $proyecto, $datos, $no_est, $razonS, $edit_id;

    public function mount()
    {
        if ($this->edit_id == false) {
            $this->clientes = Cliente::get();
        } else {
            $this->datos = RentaPendiente::find($this->edit_id);
            $this->clientes = Cliente::get();
            $this->clientes_edit = Cliente::where('id', $this->datos->id_cliente)->get();
            $this->rfc = $this->datos->rfc;
            $this->no_est = $this->datos->proyecto;
            $this->cliente = $this->datos->estacionamiento;
            $this->razonS = $this->clientes_edit[0]->razon_social;
            $this->cliente = $this->datos->estacionamiento;
            $this->cliente_id = $this->datos->id_cliente;
        }
    }

    public function updatedClienteId($value)
    {
        $this->datos = Cliente::where('id', $value)->first();
        $this->rfc = $this->datos->rfc;
        $this->no_est = $this->datos->no_estacionamiento;
        $this->razonS = $this->datos->razon_social;
        $this->cliente = $this->datos->Cliente;
        $this->cliente_id = $this->datos->id;
    }

    public function render()
    {
        return view('livewire.rentas-pendientes');
    }
}
