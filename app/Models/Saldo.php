<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Saldo
 * @package App\Models
 * @version July 16, 2020, 6:51 am UTC
 *
 * @property number $monto
 * @property string $fecha
 * @property string $autorizacion
 * @property string|\Carbon\Carbon $created_at
 * @property number $restante
 */
class Saldo extends Model
{

    public $table = 'saldos';

    public $timestamps = false;




    public $fillable = [
        'monto',
        'fecha',
        'autorizacion',
        'created_at',
        'restante'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'monto' => 'decimal:0',
        'fecha' => 'date',
        'autorizacion' => 'string',
        'created_at' => 'datetime',
        'restante' => 'decimal:0'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];


}
