<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Ingreso
 * @package App\Models
 * @version July 21, 2020, 6:54 am UTC
 *
 * @property string $fecha
 * @property number $i_operacion
 * @property number $i_cobranza
 * @property number $i_com_vta_equipo
 * @property number $e_remb_cliente
 * @property number $e_pago_prov
 * @property number $e_morrallas
 * @property number $e_rem_caja_ch
 * @property number $e_nomina
 * @property number $e_impuestos
 * @property number $p_pago_cred
 * @property number $p_arrendamientos
 * @property number $p_comi_bancarias
 */
class Ingreso extends Model
{

    public $table = 'ingresos_egresos';
    
    public $timestamps = false;




    public $fillable = [
        'fecha',
        'i_operacion',
        'i_cobranza',
        'i_com_vta_equipo',
        'e_remb_cliente',
        'e_pago_prov',
        'e_morrallas',
        'e_rem_caja_ch',
        'e_nomina',
        'e_impuestos',
        'p_pago_cred',
        'p_arrendamientos',
        'p_comi_bancarias'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fecha' => 'date',
        'i_operacion' => 'decimal:0',
        'i_cobranza' => 'decimal:0',
        'i_com_vta_equipo' => 'decimal:0',
        'e_remb_cliente' => 'decimal:0',
        'e_pago_prov' => 'decimal:0',
        'e_morrallas' => 'decimal:0',
        'e_rem_caja_ch' => 'decimal:0',
        'e_nomina' => 'decimal:0',
        'e_impuestos' => 'decimal:0',
        'p_pago_cred' => 'decimal:0',
        'p_arrendamientos' => 'decimal:0',
        'p_comi_bancarias' => 'decimal:0'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
