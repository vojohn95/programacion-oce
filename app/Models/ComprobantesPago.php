<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ComprobantesPago
 *
 * @property int $id
 * @property int|null $id_promesas
 * @property string|null $documento
 * @property float|null $monto_pago
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Promesa|null $promesa
 *
 * @package App\Models
 */
class ComprobantesPago extends Model
{
	protected $table = 'comprobantes_pagos';

	protected $casts = [
		'id_promesas' => 'int',
		'monto_pago' => 'float'
	];

	protected $fillable = [
		'id_promesas',
		'documento',
		'monto_pago',
        'fecha_pago',
        'comentario'
	];

	public function promesa()
	{
		return $this->belongsTo(Promesa::class, 'id_promesas');
	}
}
