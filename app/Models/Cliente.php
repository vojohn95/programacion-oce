<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Cliente
 * @package App\Models
 * @version July 16, 2020, 6:46 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $promesas
 * @property \Illuminate\Database\Eloquent\Collection $promesa1s
 * @property string $Cliente
 * @property string $Tipo
 */
class Cliente extends Model
{

    public $table = 'Clientes';

    public $timestamps = false;

    public $fillable = [
        'Cliente',
        'Tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Cliente' => 'string',
        'Tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function promesas()
    {
        return $this->hasMany(\App\Models\Promesa::class, 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function promesa1s()
    {
        return $this->hasMany(\App\Models\Promesa::class, 'id_cliente');
    }
}
