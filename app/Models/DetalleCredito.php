<?php

namespace App\Models;

use Eloquent as Model;



/**
 * Class DetalleCredito
 * @package App\Models
 * @version July 8, 2021, 5:49 pm UTC
 *
 * @property \App\Models\Cliente $idInstitucion
 * @property integer $id_institucion
 * @property string $no_contrato
 * @property number $monto_inicial
 * @property number $monto_pago
 * @property string $comprobante_pago
 * @property number $monto_actual
 * @property string $estatus
 * @property string|\Carbon\Carbon $created_at
 * @property string|\Carbon\Carbon $updated_at
 * @property string|\Carbon\Carbon $deleted_at
 */
class DetalleCredito extends Model
{


    public $table = 'detalle_credito';

    public $timestamps = false;




    public $fillable = [
        'id_institucion',
        'no_contrato',
        'monto_inicial',
        'monto_pago',
        'comprobante_pago',
        'monto_actual',
        'estatus',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_institucion' => 'integer',
        'no_contrato' => 'string',
        'monto_inicial' => 'decimal:2',
        'monto_pago' => 'decimal:2',
        'comprobante_pago' => 'string',
        'monto_actual' => 'decimal:2',
        'estatus' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_institucion' => 'nullable|integer',
        'no_contrato' => 'nullable|string|max:45',
        'monto_inicial' => 'nullable|numeric',
        'monto_pago' => 'nullable|numeric',
        'comprobante_pago' => 'nullable',
        'monto_actual' => 'nullable|numeric',
        'estatus' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idInstitucion()
    {
        return $this->belongsTo(\App\Models\Cliente::class, 'id_institucion');
    }
}
