<?php

namespace App\Models;

//use Eloquent as Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Promesa
 * @package App\Models
 * @version July 16, 2020, 6:47 am UTC
 *
 * @property \App\Models\Cliente $idCliente
 * @property number $promesa
 * @property number $pagado
 * @property integer $id_cliente
 * @property string $compromete_central
 * @property string $Compromete_cliente
 * @property string $Fecha_promesa
 * @property string $Fecha_pago
 * @property string $ruta
 * @property string|\Carbon\Carbon $created_at
 * @property string|\Carbon\Carbon $updated_at
 */
class Promesa extends Model
{

    public $table = 'Promesas';

    public $timestamps = false;

    public $fillable = [
        'promesa',
        //'pagado',
        'id_cliente',
        'compromete_central',
        'Compromete_cliente',
        'Fecha_promesa',
        'Fecha_pago',
        'ruta',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idPromesas' => 'integer',
        'promesa' => 'decimal:0',
        //'pagado' => 'decimal:0',
        'id_cliente' => 'integer',
        'compromete_central' => 'string',
        'Compromete_cliente' => 'string',
        'Fecha_promesa' => 'date',
        'Fecha_pago' => 'date',
        'ruta' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }

    public function comprobantes_pagos()
    {
        return $this->hasMany(ComprobantesPago::class, 'id_promesas');
    }
}
