<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class RentaPendiente
 * @package App\Models
 * @version December 21, 2021, 6:07 am UTC
 *
 * @property string $grupo
 * @property number $proyecto
 * @property integer $id_cliente
 * @property string $estacionamiento
 * @property string $concepto
 * @property string $rfc
 * @property number $monto_cxp
 * @property string $fecha_entrega
 * @property string $estatus
 * @property string $observaciones
 * @property string $gerente
 * @property string|\Carbon\Carbon $created_at
 * @property string|\Carbon\Carbon $updated_at
 */
class RentaPendiente extends Model
{
    public $table = 'rentas_pendientes';

    public $timestamps = false;

	protected $primaryKey = 'id_rentas';

	protected $dates = [
		'fecha_entrega'
	];

    public $fillable = [
        'grupo',
        'proyecto',
        'id_cliente',
        'estacionamiento',
        'concepto',
        'rfc',
        'monto_cxp',
        'fecha_entrega',
        'estatus',
        'observaciones',
        'gerente',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_rentas' => 'integer',
        'grupo' => 'string',
        'proyecto' => 'decimal:1',
        'id_cliente' => 'integer',
        'estacionamiento' => 'string',
        'concepto' => 'string',
        'rfc' => 'string',
        'monto_cxp' => 'decimal:2',
        'fecha_entrega' => 'date',
        'estatus' => 'string',
        'observaciones' => 'string',
        'gerente' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'grupo' => 'nullable|string|max:55',
        'proyecto' => 'nullable|numeric',
        'id_cliente' => 'nullable|integer',
        'estacionamiento' => 'nullable|string|max:255',
        'concepto' => 'nullable|string|max:255',
        'rfc' => 'nullable|string|max:14',
        'monto_cxp' => 'nullable|numeric',
        'fecha_entrega' => 'nullable',
        'estatus' => 'nullable|string',
        'observaciones' => 'nullable|string|max:1000',
        'gerente' => 'nullable|string|max:255',
    ];


}


