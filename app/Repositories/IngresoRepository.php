<?php

namespace App\Repositories;

use App\Models\Ingreso;
use App\Repositories\BaseRepository;

/**
 * Class IngresoRepository
 * @package App\Repositories
 * @version July 21, 2020, 6:54 am UTC
*/

class IngresoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'i_operacion',
        'i_cobranza',
        'i_com_vta_equipo',
        'e_remb_cliente',
        'e_pago_prov',
        'e_morrallas',
        'e_rem_caja_ch',
        'e_nomina',
        'e_impuestos',
        'p_pago_cred',
        'p_arrendamientos',
        'p_comi_bancarias'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ingreso::class;
    }
}
