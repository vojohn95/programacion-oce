<?php

namespace App\Repositories;

use App\Models\RentaPendiente;
use App\Repositories\BaseRepository;

/**
 * Class RentaPendienteRepository
 * @package App\Repositories
 * @version December 21, 2021, 6:07 am UTC
*/

class RentaPendienteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'grupo',
        'proyecto',
        'id_cliente',
        'estacionamiento',
        'concepto',
        'rfc',
        'monto_cxp',
        'fecha_entrega',
        'estatus',
        'observaciones',
        'gerente',

    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RentaPendiente::class;
    }
}
