<?php

namespace App\Repositories;

use App\Models\DetalleCredito;
use App\Repositories\BaseRepository;

/**
 * Class DetalleCreditoRepository
 * @package App\Repositories
 * @version July 8, 2021, 5:49 pm UTC
*/

class DetalleCreditoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_institucion',
        'no_contrato',
        'monto_inicial',
        'monto_pago',
        'comprobante_pago',
        'monto_actual',
        'estatus',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetalleCredito::class;
    }
}
