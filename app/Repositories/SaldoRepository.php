<?php

namespace App\Repositories;

use App\Models\Saldo;
use App\Repositories\BaseRepository;

/**
 * Class SaldoRepository
 * @package App\Repositories
 * @version July 16, 2020, 6:51 am UTC
*/

class SaldoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'monto',
        'fecha',
        'autorizacion',
        'created_at',
        'restante'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Saldo::class;
    }
}
